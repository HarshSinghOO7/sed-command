# Sed Command

Sed command stands for stream editor and it can perform function like searching, insertion, deletion, find and replace without opening the file.

## Searching

#### Searching according to a specific pattern

```
sed -n '/Harry/p' Harry\ Potter.txt
```

The above command will print all the lines which matches the pattern.
In the code the -n stands for print the line (echo) and the '/' is know as the delimiter. We can use other symbol like (.) also as a delimiter. And '/p' is for printing the pattern which is matched.

```
sed -n '/[HRM]/p' Harry\ Potter.txt
```

The above code will print the variable which matches the pattern and the patten is any of these characters 'H', 'R', 'M'.


## Search and Replace

#### Replacing only The first occurence

```
sed 's/Harry/Harsh/' Harry\ Potter.txt
```

This command will replace the occurence of the Harry with Harsh in the file. It will not change is there are more than one occurence within the same line. By default it will only change the first occurence and the changes won't reflect back in the actual file.

In the code the s stands for substituting.

#### Replacing all the occurence

```
sed 's/Harry/Harsh/g' Harry\ Potter.txt
```

The command with /g flag is used for replacing all the occurence from a file.

#### Replacing on a specific line number

```
sed '10 s/Harry/Harsh/g' Harry\ Potter.txt
```

As the name suggests it will replace the name to a specified line number. No other occurence will get replaced.

#### Replacing to a specific range

```
sed '2,$ s/Harry/Harsh/g' Harry\ Potter.txt
```

The command will replace all the occurence starting from line 2 till the end. We can specify the $ sign to the number of line we want to replace it to.

## Deletion

#### Deleting a line

```
sed '$d' Harry\ Potter.txt
```

In the command $d is used to delete the last line of the file. Now instead of $ we can specify any number to delete that particular line.

```
sed '100d' Harry\ Potter.txt
```


#### Deleting lines from a particular range

```
sed '10,$d' Harry\ Potter.txt
```

The 10 is the starting line and $d to delete till last only upto that particular range. We can change $ to any desired number for the range.

#### Deleting the pattern matched

```
sed '/Harry/d' Harry\ Potter.txt
```

The command will delete all the Lines that has that particular pattern.

